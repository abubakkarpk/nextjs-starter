import React from 'react'
import App from 'next/app'
import {Provider} from "react-redux";
import withReduxStore from "../lib/redux-store";

import '../css/tailwind.css'

class MyApp extends App {
  render() {
    const { Component, pageProps, reduxStore } = this.props
    return (
        <Provider store={reduxStore}>
            <Component {...pageProps} />
        </Provider>
    );
  }
}

export default withReduxStore(MyApp);