import {connect} from "react-redux";
import { loginUser } from "../store/actions/auth";

function HomePage(props) {
    return <div>Hello World</div>
}

HomePage.getInitialProps = async ({reduxStore}) => {
    await reduxStore.dispatch(loginUser());
    return {};
}

function mapStateToProps(state) {
    return {
        auth: state.auth
    }
}

export default connect(mapStateToProps)(HomePage)