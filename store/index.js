import reducer from "./reducers";
import thunk from "redux-thunk";
import {createStore, applyMiddleware} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";

export function initializeStore(initialState = {}) {
    return createStore(reducer, initialState, composeWithDevTools(applyMiddleware(thunk)));
}