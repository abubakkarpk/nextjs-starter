export const SET_AUTH_USER = "set-auth-user";
export const SET_AUTH_GUARD = "set-auth-guard";
export const SET_AUTH_TOKEN = "set-auth-token";