import {
    SET_AUTH_USER,
    SET_AUTH_GUARD,
    SET_AUTH_TOKEN
} from "./types";

function setAuthUser(user) {
    return {type: SET_AUTH_USER, payload: user};
}

function setAuthGuard(guard) {
    return {type: SET_AUTH_GUARD, payload: guard};
}

function setAuthToken(token) {
    return {type: SET_AUTH_TOKEN, payload: token};
}

export function loginUser() {
    return dispatch => {
        return new Promise(resolve => {
            setTimeout(() => {
                dispatch(setAuthUser({name: "Abu Bakkar"}));
                dispatch(setAuthGuard("user"));
                dispatch(setAuthToken("1vrtwtwetwetwtew656e4168wer17e81t86et86"));
                resolve();
            }, 1000)
        })
    }
}

export function logoutUser() {
    return dispatch => {
        setTimeout(() => {
            dispatch(setAuthUser(null));
            dispatch(setAuthToken(null));
        }, 1000)
    }
}