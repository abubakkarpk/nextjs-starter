import {
    SET_AUTH_USER,
    SET_AUTH_GUARD,
    SET_AUTH_TOKEN
} from "../actions/types";

const initialState = {
    user: null,
    guard: "",
    token: "",
}

export default function (state = initialState, action) {
    switch(action.type) {
        case SET_AUTH_USER:
            return {...state, user: action.payload};
        case SET_AUTH_GUARD:
            return {...state, guard: action.payload};
        case SET_AUTH_TOKEN:
            return {...state, token: action.payload};
        default:
            return state;
    }
}